[![pipeline status](https://gitlab.cern.ch/GeoModelDev/documentation/badges/main/pipeline.svg)](https://gitlab.cern.ch/GeoModelDev/documentation/commits/main)


# GeoModel - A Detector Description Toolkit for HEP experiments

This repository contains the source for the website hosted at: https://www.cern.ch/geomodel


