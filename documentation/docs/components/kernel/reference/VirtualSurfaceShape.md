

##	Virtual Surface Shape


### Introduction

The virtual surface shape classes are designed to describe the geometry of two dimensional virtual surfaces. The specific surface shape classes are used as input variables of `GeoVSurface`, as described in section [GeoVSurface](#geovsurface). The set of surfaces is extensible.



In the table here below, a list of the existing virtual surface shapes in the GeoModelKernel package is given:


| Class   | Virtual Surface Shape |
| ------- | ----- |
| [GeoRectSurface](#georectsurface)  | Rectangle |
| [GeoTrapezoidSurface](#geotrapezoidsurface) | Trapezoid |
| [GeoDiamondSurface](#geodiamondsurface) | Diamond |
| [GeoAnnulusSurface](#geoannulussurface) | Annulus / Circular Sector |

The surface shapes can be visualized by *gmex*, and some screenshots appear in the following documentation.


{% include 'components/kernel/reference/RCBase/GeoVSurfaceShape/GeoVSurfaceShape.md' %}

{% include 'components/kernel/reference/RCBase/GeoVSurfaceShape/GeoRectSurface.md' %}

{% include 'components/kernel/reference/RCBase/GeoVSurfaceShape/GeoTrapezoidSurface.md' %}

{% include 'components/kernel/reference/RCBase/GeoVSurfaceShape/GeoDiamondSurface.md' %}

{% include 'components/kernel/reference/RCBase/GeoVSurfaceShape/GeoAnnulusSurface.md' %}


